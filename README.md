# max_pkg

# How to install this package?
pip install max-pkg

# If you set up the mirror source in Pypi, you should use the following command:
pip install -i https://pypi.org/simple max-pkg

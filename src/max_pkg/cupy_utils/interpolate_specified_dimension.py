#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：interpolate_specified_dimension.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/25 14:04 
@Brief   ：
"""

import cupy as cp
from cupyx.scipy.interpolate import PchipInterpolator
from Project.max_pkg.numpy_utils.move_element import move_element


def interpolate_specified_dimension(data, target_length, axis=1):
    """
    将多维数组 data 的指定维度插值到指定的目标长度。

    参数：
    data: cp.ndarray
        输入的多维数组。
    target_length: int
        插值后的目标长度。
    axis: int, optional
        需要插值的维度索引（默认为1，即第二个维度）。

    返回：
    interpolated_data: cp.ndarray
        在指定维度插值后的数组。
    """
    # 获取输入数据的形状
    original_shape = list(data.shape)

    # 改变轴的顺序
    num_dimensions = data.ndim
    axis_order = list(range(num_dimensions))
    # 将指定维度移到末尾
    data = cp.transpose(data, move_element(axis_order, axis, -1))
    permuted_shape = list(data.shape)
    data = cp.reshape(data, (-1, original_shape[axis]))

    # 创建插值函数
    original_length = original_shape[axis]
    x_original = cp.linspace(0, 1, original_length)
    x_target = cp.linspace(0, 1, target_length)

    interpolated_data = cp.zeros((data.shape[0], target_length))

    # 对每个 i 行应用一维插值
    for i in range(data.shape[0]):
        interpolator = PchipInterpolator(x_original, data[i, :])
        interpolated_data[i, :] = interpolator(x_target)

    # 恢复成原始形状
    permuted_shape[-1] = target_length
    interpolated_data = cp.reshape(interpolated_data, permuted_shape)
    interpolated_data = cp.transpose(interpolated_data, move_element(axis_order, -1, axis))

    return interpolated_data


if __name__ == '__main__':
    # 示例用法
    data = cp.random.rand(3, 4, 3, 3, 5)  # 一个3x4x3x3x5的五维数组
    target_length = 400  # 目标长度
    axis = 4  # 需要插值的维度（第五个维度）
    interpolated_data = interpolate_specified_dimension(data, target_length, axis)
    print(interpolated_data.shape)  # 输出应该是 (3, 4, 3, 3, 400)


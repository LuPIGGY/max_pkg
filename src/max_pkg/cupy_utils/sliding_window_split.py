#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：sliding_window_split.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/25 15:51 
@Brief   ：
"""

import cupy as cp


def sliding_window_split(data, window_size, step_size=1):
    """
    在 y 维度上对二维数组进行滑动窗口分割，并允许指定步长。

    参数:
    data (cupy.ndarray): 输入的二维数组，形状为 (x, y)。
    window_size (int): 滑动窗口的大小。
    step_size (int, 可选): 滑动窗口的步长。默认为 1。

    返回:
    cupy.ndarray: 分割后的三维数组，形状为 (x, n, window_size)，其中 n 是子数组的数量。
    """
    x, y = data.shape

    # 确保 y 维度足够长以容纳至少一个窗口
    if y < window_size:
        raise ValueError("Window size is larger than the length of the y dimension.")

    # 计算滑动后得到的子数组的数量
    n = (y - window_size) // step_size + 1

    # 使用列表推导式和切片来提取窗口
    windows = cp.array([data[:, i:i + window_size] for i in range(0, y - window_size + 1, step_size)])

    # 确保输出的形状为 (x, n, window_size)
    # 注意：这里 windows 的形状实际上是 (n, x, window_size)
    # 所以我们需要再次转置以得到 (x, n, window_size)
    return windows.transpose(1, 0, 2)  # 将 (n, x, window_size) 转置为 (x, n, window_size)


# 示例用法
if __name__ == '__main__':
    # 创建一个示例数组
    data = cp.random.rand(3, 10)

    # 设置窗口大小和步长
    window_size = 4
    step_size = 2

    # 进行滑动窗口分割
    result = sliding_window_split(data, window_size, step_size)

    # 打印结果
    print("Original array shape:", data.shape)
    print("Resulting array shape:", result.shape)
    print("Resulting array:")
    print(result)

#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：random_crop.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/25 13:41 
@Brief   ：
"""
import cupy as cp


def random_crop(data, crop_dim, crop_range=[0.7, 1], min_length=70):
    """
    使用CuPy对数据进行随机裁剪。

    参数:
    data (cupy.ndarray): 输入数据，形状为任意，且应该在GPU上。
    crop_dim (int): 需要裁剪的维度索引。
    crop_range (list or tuple): 裁剪长度的比例范围，默认为 [0.5, 1]。
    min_length (int): 裁剪后的最小长度，默认为 70。

    返回:
    cupy.ndarray: 裁剪后的数据，保持原来的维度数，且在GPU上。
    """
    # 获取指定裁剪维度的原始长度
    len_origin = data.shape[crop_dim]

    # 计算随机裁剪系数
    crop_alpha = cp.random.rand() * (crop_range[1] - crop_range[0]) + crop_range[0]

    # 计算裁剪后的长度
    len_cropped = int(cp.floor(crop_alpha * len_origin))

    # 限定长度下限和上限
    len_cropped = cp.maximum(len_cropped, min_length)
    len_cropped = cp.minimum(len_cropped, len_origin)
    # 注意：这里需要将CuPy数组转换为NumPy数组或使用.item()来获取Python标量，因为后续操作需要整数索引
    len_cropped = len_cropped.item()  # 或者使用 int(len_cropped) 如果CuPy 13.x不直接支持.item()

    # 设置一个裁剪起始位置偏移量
    offset = cp.random.randint(0, len_origin - len_cropped + 1)
    offset = offset.item()  # 同样需要转换为Python标量

    # 创建一个切片对象，用于裁剪数据
    slices = [slice(None)] * data.ndim
    slices[crop_dim] = slice(offset, offset + len_cropped)

    # 对数据进行裁剪
    cropped_data = data[tuple(slices)]

    return cropped_data


if __name__ == "__main__":
    import cupy as cp

    # 示例数据
    # 注意：在实际使用中，你需要确保data已经在GPU上。可以使用cp.asarray(np_array)来将NumPy数组转移到GPU。
    data = cp.random.rand(10, 500)

    # 在指定的维度上进行裁剪（这里假设是第二个维度，即channels维度，但具体要看你的数据如何解释）
    # 注意：根据你的数据维度和裁剪需求，可能需要调整crop_dim的值
    cropped_data = random_crop(data, crop_dim=1, crop_range=[0.7, 1], min_length=70)

    print(cropped_data.shape)  # 输出裁剪后的数据形状

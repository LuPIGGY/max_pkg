#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：transformer 
@File    ：plot_confusion_matrices.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/11 14:49 
@Brief   ：
"""


import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns


def plot_confusion_matrices(cm, behaviour, filename='confusion_matrices.png', show_plot=True):
    """
    绘制原始和归一化的混淆矩阵，并保存为图像文件。

    :param cm: 原始混淆矩阵 (numpy array)
    :param behaviour: 行为标签列表 (list of str)
    :param filename: 保存的图像文件名 (str)
    :param show_plot: 是否显示图像 (bool)
    """
    # 设置Matplotlib以使用支持中文的字体
    plt.rcParams['font.sans-serif'] = ['SimHei']  # 指定默认字体
    plt.rcParams['axes.unicode_minus'] = False  # 解决保存图像时负号'-'显示为方块的问题
    plt.rcParams['font.size'] = 16  # 这将全局设置所有文本的字体大小为16

    # 将混淆矩阵转换为百分比形式
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    # 创建图形和子图
    fig, axes = plt.subplots(1, 2, figsize=(20, 7))

    # 绘制原始混淆矩阵
    sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', ax=axes[0], xticklabels=behaviour, yticklabels=behaviour)
    axes[0].set_xlabel('Predicted')
    axes[0].set_ylabel('Truth')
    axes[0].set_title('原始混淆矩阵')

    # 绘制归一化后的混淆矩阵
    sns.heatmap(cm_normalized, annot=True, fmt='.2%', cmap='Blues', ax=axes[1], xticklabels=behaviour, yticklabels=behaviour)
    axes[1].set_xlabel('Predicted')
    axes[1].set_ylabel('Truth')
    axes[1].set_title('归一化混淆矩阵 (百分比)')

    # 调整布局
    plt.tight_layout()

    # 保存为图像文件
    plt.savefig(filename, dpi=300)

    # 显示图像
    if show_plot:
        plt.show()

    # 关闭图像窗口（可选）
    plt.close()


if __name__ == '__main__':
    # 示例用法
    # 加载混淆矩阵
    cm = np.load('../TSception_confusion_matrix.npy')

    # 行为标签
    behaviour = ["摘眼镜", "摘帽子", "摘手套", "脱披肩", "验电行为"]

    # 调用函数绘制混淆矩阵，不显示图像
    plot_confusion_matrices(cm, behaviour, filename='../TSception_confusion_matrix_comparison.png', show_plot=True)
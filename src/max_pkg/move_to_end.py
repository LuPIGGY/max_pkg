#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：action_recognition 
@File    ：move_to_end.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/17 11:07 
@Brief   ：
"""


def move_to_end(arr, value):
    """
    将数组中的指定值移动到数组最末尾，其他元素顺序不变。

    参数:
    arr (list): 输入的数组。
    value: 要移动到末尾的值。

    返回:
    list: 移动值后的新数组。
    """
    # 找到值的索引
    index = arr.index(value) if value in arr else None

    # 如果值不在数组中，直接返回原数组（或者可以抛出异常）
    if index is None:
        return arr

    # 使用切片和连接操作重新排列数组
    new_arr = arr[:index] + arr[index + 1:] + [value]

    return new_arr


if __name__ == '__main__':
    # 示例使用
    original_array = list(range(11))  # [0, 1, 2, ..., 10]
    value_to_move = 2

    transformed_array = move_to_end(original_array, value_to_move)
    print(transformed_array)  # 输出应该是 [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 5]

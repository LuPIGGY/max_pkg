#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：sliding_window_split.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/18 15:45 
@Brief   ：
"""

import numpy as np


def sliding_window_split(data, window_size, step_size=1):
    """
    在y维度上对二维数组进行滑动窗口分割，并允许指定步长。

    参数:
    data (numpy.ndarray): 输入的二维数组，形状为(x, y)。
    window_size (int): 滑动窗口的大小。
    step_size (int, 可选): 滑动窗口的步长。默认为1。

    返回:
    numpy.ndarray: 分割后的三维数组，形状为(x, n, window_size)，其中n是子数组的数量。
    """
    x, y = data.shape

    # 确保y维度足够长以容纳至少一个窗口
    if y < window_size:
        raise ValueError("Window size is larger than the length of the y dimension.")

    # 计算滑动后得到的子数组的数量
    n = (y - window_size) // step_size + 1

    # 使用列表推导式和切片来提取窗口
    windows = np.array([data[:, i:i + window_size] for i in range(0, y - window_size + 1, step_size)])

    # 确保输出的形状为(x, n, window_size)
    # 注意：这里windows的形状实际上是(n, x, window_size)（但它是从(n, window_size, x)转置前的切片得到的）
    # 所以我们需要再次转置以得到(x, n, window_size)
    return windows.transpose(1, 0, 2)  # 将(n, x, window_size)转置为(x, n, window_size)


if __name__ == '__main__':
    # 示例用法（使用非优化版本，因为它更简单且容易理解）
    data = np.array([[1, 2, 3, 4, 5, 6],
                     [7, 8, 9, 10, 11, 12]])
    print(data.shape)
    window_size = 5
    step_size = 1
    result = sliding_window_split(data, window_size, step_size)
    print(result.shape)

# 对连续异常零值进行插值处理(处理多维度数组(123, 12, 2)， 只能对第一个维度进行插值处理)
import numpy as np
import torch
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d


# # 定义一个函数来插值零值
# def interpolate_zeros(array, axis=0):
#     # 获取数组的形状
#     shape = array.shape
#
#     # 初始化一个用于存储结果的数组（与输入数组形状相同）
#     result = np.empty_like(array)
#
#     # 遍历需要插值的维度
#     for i in range(shape[1]):  # 遍历第二个维度（这里我们假设第二个维度是特征维度，不需要插值）
#         for j in range(shape[2]):  # 遍历第三个维度（同样假设不需要插值）
#             # 提取当前特征/通道的一维数据
#             slice_data = array[:, i, j]
#
#             # 找到非零值的位置
#             non_zero_indices = np.where(slice_data != 0)[0]
#
#             # 如果全是零值，则跳过插值（或者你可以选择填充一个默认值）
#             if len(non_zero_indices) > 1:
#                 # 使用线性插值
#                 x_vals = np.arange(len(slice_data))  # 原始数据的x坐标
#                 y_vals_non_zero = slice_data[non_zero_indices]  # 非零值的y坐标
#                 x_vals_non_zero = non_zero_indices  # 非零值对应的x坐标
#
#                 # 使用NumPy的interp函数进行插值
#                 interpolated_vals = np.interp(x_vals, x_vals_non_zero, y_vals_non_zero)
#
#                 # 将插值结果放回结果数组
#                 result[:, i, j] = interpolated_vals
#             else:
#                 # 如果全是零值或只有一个非零值，则直接复制原数据（或填充一个默认值）
#                 result[:, i, j] = slice_data
#
#     return result

def interpolate_zeros(array, axis=0, method='linear'):  # axis=0不起作用
    """
      对数组中的零值进行插值处理。

      参数:
      array (numpy.ndarray): 输入数组，其中可能包含零值。
      axis (int): 指定要进行插值的维度。然而，当前实现仅支持对最后一个维度之前的维度进行插值（即假设数据是时间序列或空间序列，并且这些序列是数组的前几个维度）。
                 注意：由于当前实现方式，`axis` 参数实际上不起作用，插值总是在最后一个维度之前的维度上进行（即假设数据是 (..., sequence_length) 形状）。
      method (str): 插值方法，默认为 'linear'。可以是 'linear', 'nearest', 'zero', 'slinear', 'quadratic', 'cubic' 等 scipy.interpolate.interp1d 支持的方法。

      返回:
      numpy.ndarray: 插值后的数组，与输入数组形状相同。零值被插值后的值替换。

      注意:
      - 当前实现假设输入数组至少有两个维度，并且插值在倒数第二个维度上进行（即假设数据是形如 (..., sequence_length) 的序列）。
      - 如果某个序列全是零值或只有一个非零值，则插值结果将是该序列的原始值（或可以选择填充一个默认值，但当前实现直接复制原数据）。
      - `axis` 参数目前不起作用，未来版本可能会改进以支持任意维度的插值。
    """
    # 获取数组的形状
    shape = array.shape

    # 初始化一个用于存储结果的数组（与输入数组形状相同）
    result = np.empty_like(array)

    # 遍历需要插值的维度（这里假设是第一个维度，即时间序列或空间序列）
    for i in range(shape[1]):  # 遍历第二个维度（特征维度）
        for j in range(shape[2]):  # 遍历第三个维度（通道维度）
            # 提取当前特征/通道的一维数据
            slice_data = array[:, i, j]

            # 找到非零值的位置
            non_zero_indices = np.where(slice_data != 0)[0]

            # 如果全是零值，则直接复制原数据（或可以选择填充一个默认值）
            if len(non_zero_indices) > 1:
                # 提取非零值及其对应的索引
                x_vals_non_zero = non_zero_indices
                y_vals_non_zero = slice_data[non_zero_indices]

                # 创建 interp1d 插值对象
                f_interp = interp1d(x_vals_non_zero, y_vals_non_zero, kind=method, bounds_error=False, fill_value=0)
                # 注意：bounds_error=False 和 fill_value=0 表示如果插值点超出已知数据点范围，则返回0（即不进行外插）

                # 对所有索引进行插值
                x_vals = np.arange(len(slice_data))
                interpolated_vals = f_interp(x_vals)

                # 将插值结果放回结果数组
                result[:, i, j] = interpolated_vals
            else:
                # 如果全是零值或只有一个非零值，则直接复制原数据
                result[:, i, j] = slice_data

    return result


if __name__ == "__main__":
    # ---------------------------------------------------------------------begin 加载数据
    # 示例数据
    # 假设data是你的三维数据
    # data = np.random.randint(0, 10, size=(128, 12, 2))  # 这里我用随机数生成了一个示例数据，你需要用你的实际数据替换它
    #
    # # 为了演示，我们故意在一些位置设置零值
    # data[::10, :, :] = 0  # 每10个元素中的第一个元素设置为0（这是一个示例，实际情况可能不同）

    # 导入YOLO提取的关键点数据
    file_path = r"D:\行为数据——关键点\空中作业场景\3 脱披肩\0号\video_20231023_162633.mp4.pth"
    data = torch.load(file_path)
    data = data.cpu().numpy()  # (378, 12, 2) # 只有12个key_point的坐标数据
    key_point = 0  # 选择关键点， 参考Coco_keypoint_indexes中的对应关系
    key_point_axis = 0  # 选择坐标的曲线 0：x坐标， 1：y坐标
    data = data[:]
    # ---------------------------------------------------------------------end 加载数据

    # ----------------------------------------------------------------------begin 可视化插值前的数据
    # 绘制折线图，确保索引符合数据的维度
    plt.figure(figsize=(10, 5))
    plt.plot(data[:, 0, 1], label=f"raw", linestyle='-')
    # 调整布局和显示图表
    plt.legend()
    plt.tight_layout()
    plt.show()
    # ----------------------------------------------------------------------end 可视化插值前的数据

    # ----------------------------------------------------------------------begin 插值处理
    # 对数据进行插值处理
    interpolated_data = interpolate_zeros(data)
    # ----------------------------------------------------------------------end 插值处理

    # ----------------------------------------------------------------------begin 可视化插值后的数据
    # 绘制折线图，确保索引符合数据的维度
    plt.figure(figsize=(10, 5))
    plt.plot(interpolated_data[:, 0, 1], label=f"interpolated_data", linestyle='-')
    # 调整布局和显示图表
    plt.legend()
    plt.tight_layout()
    plt.show()
    # ----------------------------------------------------------------------end 可视化插值后的数据

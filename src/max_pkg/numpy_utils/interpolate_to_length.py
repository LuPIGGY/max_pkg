# 将序列插值到指定长度(如多为数组为(128, 12, 2)，将其第一个维度通过插值得到(300, 12, 2))

import numpy as np
from scipy.interpolate import interp1d


def interpolate_to_length(data, target_shape):
    """
    将一个三维数组 data 插值到指定的 target_shape。

    参数：
    data: np.ndarray
        形状为 (112, 12, 2) 的输入数组。
    target_shape: tuple
        插值后的目标形状，例如 (200, 12, 2)。

    返回：
    interpolated_data: np.ndarray
        形状为 target_shape 的插值后的数组。
    """
    # 原始数组的第一个维度长度
    original_length = data.shape[0]

    # 创建插值函数
    x_original = np.linspace(0, 1, original_length)
    x_target = np.linspace(0, 1, target_shape[0])

    interpolated_data = np.zeros(target_shape)

    # 对每个 (i, j) 列应用一维插值
    for i in range(data.shape[1]):
        for j in range(data.shape[2]):
            f = interp1d(x_original, data[:, i, j], kind='linear')
            interpolated_data[:, i, j] = f(x_target)

    return interpolated_data


if __name__ == "__main__":
    # 示例
    # data = np.random.rand(112, 12, 2)  # 形状为 (112, 12, 2) 的随机数据
    # target_shape = (200, 12, 2)
    # interpolated_data = interpolate_array(data, target_shape)
    # print(interpolated_data.shape)  # 应该输出 (200, 12, 2)

    import torch

    # 导入YOLO提取的关键点数据
    file_path = r"D:\行为数据——关键点\空中作业场景\0 摘眼镜\0号\video_20231023_162608.mp4.pth"
    data = torch.load(file_path)
    print(data.cpu().numpy().shape)
    data = data.cpu().numpy()

    # 导入可视化库plt
    import matplotlib.pyplot as plt

    # 创建包含两幅图的布局
    # 绘制折线图，确保索引符合数据的维度
    plt.figure(figsize=(10, 5))

    # 绘制 result 的两个序列
    plt.plot(data[:, 0, 0], label="nose_x", linestyle='-')
    plt.plot(data[:, 0, 1], label="nose_y", linestyle='-')

    plt.plot(data[:, 1, 0], label="left_eye_x", linestyle='-')
    plt.plot(data[:, 1, 1], label="left_eye_y", linestyle='-')

    plt.plot(data[:, 7, 0], label="left_elbow_x", linestyle='-')
    plt.plot(data[:, 7, 1], label="left_elbow_y", linestyle='-')

    # 调整布局和显示图表
    plt.legend()
    plt.tight_layout()
    plt.show()

    result = interpolate_to_length(data, (300, 12, 2))
    print(result.shape)

    # 选择一个key_point， 绘制其x,y坐标随时间的变化
    import matplotlib.pyplot as plt

    # 创建包含两幅图的布局
    # 绘制折线图，确保索引符合数据的维度
    plt.figure(figsize=(10, 5))

    # 绘制 result 的两个序列

    plt.plot(result[:, 0, 0], label="nose_x", linestyle='-')
    plt.plot(result[:, 0, 1], label="nose_y", linestyle='-')

    plt.plot(result[:, 1, 0], label="left_eye_x", linestyle='-')
    plt.plot(result[:, 1, 1], label="left_eye_y", linestyle='-')

    plt.plot(result[:, 7, 0], label="left_elbow_x", linestyle='-')
    plt.plot(result[:, 7, 1], label="left_elbow_y", linestyle='-')

    # 调整布局和显示图表
    plt.legend()
    plt.tight_layout()
    plt.show()

#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：action_recognition 
@File    ：rotate_points.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/17 16:21 
@Brief   ：
"""

import numpy as np
from .generate_rotation_matrices import generate_rotation_matrices


def rotate_points(points, theta_max):
    """
    对输入数据进行绕Z轴、Y轴和X轴的随机旋转。

    参数:
    points (np.ndarray): 形状为 (n, 3) 的输入数据，其中 n 是数据点的数量。
    theta_max (float): 旋转角度的最大范围（以弧度为单位）。

    返回:
    np.ndarray: 旋转后的数据，形状与输入数据相同。
    """
    # 生成旋转矩阵
    R_x, R_y, R_z = generate_rotation_matrices(theta_max)

    # 组合旋转矩阵（注意顺序：R_z * R_y * R_x）
    R_final = np.dot(np.dot(R_z, R_y), R_x)

    # 对每个点应用旋转矩阵（使用矩阵乘法）
    # 注意：points.T 是将点集从 (n, 3) 转置为 (3, n)，然后与 R_final (3, 3) 相乘得到旋转后的 (3, n)，最后再转置回 (n, 3)
    rotated_points = np.dot(R_final, points.T).T

    return rotated_points


if __name__ == '__main__':
    # 示例使用
    np.random.seed(0)  # 设置随机种子以便结果可重复
    points = np.random.rand(300, 3)  # 生成一些随机点作为示例
    theta_max = np.pi / 4  # 设置旋转角度的最大范围为45度（π/4弧度）

    rotated_points = rotate_points(points, theta_max)
    print("Original points:\n", points.shape)
    print("Rotated points:\n", rotated_points.shape)

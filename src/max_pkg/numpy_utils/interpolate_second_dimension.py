#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：action_recognition 
@File    ：interpolate_second_dimension.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/16 18:16 
@Brief   ：
"""

import numpy as np
from scipy.interpolate import interp1d


def interpolate_second_dimension(data, target_shape):
    """
    将一个二维数组 data 的第二个维度插值到指定的目标形状。

    参数：
    data: np.ndarray
        形状为 (x, y) 的输入数组。
    target_shape: tuple
        插值后的目标形状，例如 (x, 200)。

    返回：
    interpolated_data: np.ndarray
        形状为 (x, target_shape[1]) 的插值后的数组。
    """
    # 原始数组的第二个维度长度
    original_length = data.shape[1]

    # 创建插值函数
    x_original = np.linspace(0, 1, original_length)
    x_target = np.linspace(0, 1, target_shape[1])

    interpolated_data = np.zeros((data.shape[0], target_shape[1]))

    # 对每个 i 行应用一维插值
    for i in range(data.shape[0]):
        f = interp1d(x_original, data[i, :], kind='linear')
        interpolated_data[i, :] = f(x_target)

    return interpolated_data


# 示例用法
if __name__ == "__main__":
    # 创建一个形状为 (10, 12) 的示例数组
    data = np.random.rand(10, 12)

    # 目标形状 (10, 200)
    target_shape = (10, 200)

    # 进行插值
    interpolated_data = interpolate_second_dimension(data, target_shape)

    print(interpolated_data.shape)  # 输出: (10, 200)

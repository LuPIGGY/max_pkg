import numpy as np


def normalize_along_axis(data, axis, epsilon=1e-7):
    """
    对指定轴上的数据进行归一化处理。

    参数:
    data (numpy.ndarray): 输入数据，一个多维数组。
    axis (int): 要进行归一化的轴。
    epsilon (float, 可选): 一个小的正数，用于避免除以零的错误，默认为 1e-7。

    返回:
    normalized_data (numpy.ndarray): 归一化后的数据，形状与输入数据相同。
    """
    # 确保输入是 NumPy 数组
    data = np.array(data, dtype=np.float32)

    # 沿着指定轴计算最小值和最大值
    min_val = np.min(data, axis=axis, keepdims=True)
    max_val = np.max(data, axis=axis, keepdims=True)

    # 应用归一化公式
    # 注意：这里使用 keepdims=True 来保持结果的维度与原始数据相同，以便进行广播
    normalized_data = (data - min_val) / (max_val - min_val + epsilon)

    return normalized_data


if __name__ == "__main__":
    import torch
    import matplotlib.pyplot as plt

    # 导入YOLO提取的关键点数据
    file_path = r"D:\行为数据——关键点\空中作业场景\0 摘眼镜\0号\video_20231023_162608.mp4.pth"
    data = torch.load(file_path)
    data = data.cpu().numpy()
    print(data.shape)  # (135, 12, 2)

    # 绘制折线图，确保索引符合数据的维度
    plt.figure(figsize=(10, 5))

    plt.plot(data[:120, 10, 0], label="right_wrist_x", linestyle='-')
    plt.plot(data[:120, 10, 1], label="right_wrist_y", linestyle='-')

    # 调整布局和显示图表
    plt.legend()
    plt.tight_layout()
    plt.show()

    # ------------------------------------------------------------------归一化
    # # 计算第一个维度的最小值和最大值
    # min_val = np.min(data, axis=0)
    # max_val = np.max(data, axis=0)
    #
    # # 防止除以零的情况，检查最大值和最小值是否相同（虽然在实际随机数据中不太可能）
    # min_val[min_val == max_val] += np.finfo(float).eps
    #
    # # 进行归一化
    # normalized_data = (data - min_val) / (max_val - min_val)

    # 调用函数进行归一化
    # 变换数据维度
    data = np.transpose(data, (2, 1, 0))
    # 对指定维度axis=2进行归一化
    normalized_data = normalize_along_axis(data, axis=2)
    # 恢复数据维度顺序
    normalized_data = np.transpose(normalized_data, (2, 1, 0))
    # ------------------------------------------------------------------归一化

    # 绘制折线图，确保索引符合数据的维度
    plt.figure(figsize=(10, 5))

    plt.plot(normalized_data[:120, 10, 0], label="nose_x", linestyle='-')
    plt.plot(normalized_data[:120, 10, 1], label="nose_y", linestyle='-')

    # 调整布局和显示图表
    plt.legend()
    plt.tight_layout()
    plt.show()

#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：action_recognition 
@File    ：random_crop.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/16 16:48 
@Brief   ：
"""


import numpy as np


def random_crop(data, crop_dim, crop_range=[0.5, 1], min_length=70):
    """
    对数据进行随机裁剪。

    参数:
    data (numpy.ndarray): 输入数据，形状为任意。
    crop_dim (int): 需要裁剪的维度索引。
    crop_range (list or tuple): 裁剪长度的比例范围，默认为 [0.5, 1]。
    min_length (int): 裁剪后的最小长度，默认为 70。

    返回:
    numpy.ndarray: 裁剪后的数据，保持原来的维度数。
    """
    # 获取指定裁剪维度的原始长度
    len_origin = data.shape[crop_dim]

    # 计算随机裁剪系数
    crop_alpha = np.random.rand() * (crop_range[1] - crop_range[0]) + crop_range[0]

    # 计算裁剪后的长度
    len_cropped = int(np.floor(crop_alpha * len_origin))

    # 限定长度下限和上限
    len_cropped = np.maximum(len_cropped, min_length)
    len_cropped = np.minimum(len_cropped, len_origin)

    # 设置一个裁剪起始位置偏移量
    offset = np.random.randint(0, len_origin - len_cropped + 1)

    # 创建一个切片对象，用于裁剪数据
    slices = [slice(None)] * data.ndim
    slices[crop_dim] = slice(offset, offset + len_cropped)

    # 对数据进行裁剪
    cropped_data = data[tuple(slices)]

    return cropped_data


if __name__ == "__main__":
    # 示例数据
    data = np.random.rand(10, 500)  # 形状为 (batch_size, channels, height, width)

    # 在 height 维度上进行裁剪
    cropped_data = random_crop(data, crop_dim=1, crop_range=[0.2, 1], min_length=70)

    print(cropped_data.shape)  # 输出裁剪后的数据形状

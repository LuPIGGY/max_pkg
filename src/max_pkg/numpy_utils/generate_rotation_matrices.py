#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：action_recognition 
@File    ：generate_rotation_matrices.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/17 16:14 
@Brief   ：
"""

import numpy as np


def generate_rotation_matrices(theta_max):
    """
    生成绕X轴、Y轴和Z轴旋转的3x3旋转矩阵。

    参数:
    theta_max (float): 旋转角度的最大范围（以弧度为单位）。

    返回:
    list of np.ndarray: 包含绕X轴、Y轴和Z轴旋转的3x3旋转矩阵的列表。
    """
    # 生成随机角度（在[-theta_max, theta_max]范围内）
    random_angle_x = np.random.uniform(-theta_max, theta_max)
    random_angle_y = np.random.uniform(-theta_max, theta_max)
    random_angle_z = np.random.uniform(-theta_max, theta_max)

    # 计算余弦和正弦值
    cos_theta_x, sin_theta_x = np.cos(random_angle_x), np.sin(random_angle_x)
    cos_theta_y, sin_theta_y = np.cos(random_angle_y), np.sin(random_angle_y)
    cos_theta_z, sin_theta_z = np.cos(random_angle_z), np.sin(random_angle_z)

    # 构造绕X轴旋转的矩阵
    rotation_matrix_x = np.array([
        [1, 0, 0],
        [0, cos_theta_x, -sin_theta_x],
        [0, sin_theta_x, cos_theta_x]
    ])

    # 构造绕Y轴旋转的矩阵
    rotation_matrix_y = np.array([
        [cos_theta_y, 0, sin_theta_y],
        [0, 1, 0],
        [-sin_theta_y, 0, cos_theta_y]
    ])

    # 构造绕Z轴旋转的矩阵
    rotation_matrix_z = np.array([
        [cos_theta_z, -sin_theta_z, 0],
        [sin_theta_z, cos_theta_z, 0],
        [0, 0, 1]
    ])

    # 将旋转矩阵存储在一个列表中并返回
    return [rotation_matrix_x, rotation_matrix_y, rotation_matrix_z]


if __name__ == '__main__':
    # 使用函数并打印旋转矩阵
    rotation_matrices = generate_rotation_matrices(0.3)
    for i, matrix in enumerate(rotation_matrices, start=1):
        print(f"Rotation matrix around axis {chr(ord('X') + i - 1)}:\n{matrix}\n")

    R_x, R_y, R_z = rotation_matrices
    # 将旋转矩阵相乘（注意顺序：R_z * R_y * R_x）
    R_final = np.dot(np.dot(R_z, R_y), R_x)

    # 打印最终的旋转矩阵
    print("Final rotation matrix:")
    print(R_final)

#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：action_recognition 
@File    ：move_element.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/17 14:18 
@Brief   ：
"""


def move_element(lst, from_index, to_index):
    """
    将列表中指定索引的元素取出，并重新插入到其他位置，不修改原列表。

    参数：
    lst: list
        输入列表。
    from_index: int
        要移动的元素的索引，支持负数索引。
    to_index: int
        要插入的新位置的索引，支持负数索引。

    返回：
    new_lst: list
        修改后的新列表。
    """
    # 将负数索引转换为正数索引
    if from_index < 0:
        from_index += len(lst)
    if to_index < 0:
        to_index += len(lst)

    # 检查索引是否有效
    if from_index < 0 or from_index >= len(lst):
        raise IndexError("from_index out of range")
    if to_index < 0 or to_index > len(lst):
        raise IndexError("to_index out of range")

    # 创建一个新的列表副本
    new_lst = lst.copy()

    # 取出指定索引的元素
    element = new_lst.pop(from_index)

    # 将元素插入到新位置
    new_lst.insert(to_index, element)

    return new_lst


# 示例用法
if __name__ == "__main__":
    lst = [1, 2, 3, 4, 5]
    from_index = 0  # 索引为 2 的元素（即 3）
    to_index = -1  # 索引为 4 的位置

    new_lst = move_element(lst, from_index, to_index)
    print("Original list:", lst)  # 输出: Original list: [1, 2, 3, 4, 5]
    print("New list:", new_lst)  # 输出: New list: [1, 2, 4, 5, 3]

#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：MemmapSaver.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/20 13:52 
@Brief   ：
"""
import os
import json
import numpy as np


class MemmapSaver:
    def __init__(self, dtype, shape_per_item, folder_path='./data', chunk_size=10000, base_filename='data_chunk',
                 metadata_filename='metadata.json', mode='w+'):
        self.dtype = dtype
        self.shape_per_item = shape_per_item
        self.chunk_size = chunk_size
        self.folder_path = folder_path
        self.base_filename = base_filename
        self.metadata_filename = os.path.join(folder_path, metadata_filename)
        self.mode = mode
        self.chunk_index = 0
        self.current_memmap = None
        self.offset = 0
        self.total_items = 0  # 初始化总数为0

        # Calculate the full shape of the memmap, including the chunk size dimension
        self.full_shape = (chunk_size,) + shape_per_item

        # Ensure the folder exists
        os.makedirs(folder_path, exist_ok=True)

    def _create_new_memmap(self):
        filename = os.path.join(self.folder_path, f"{self.base_filename}_{self.chunk_index}.dat")
        self.current_memmap = np.memmap(filename, dtype=self.dtype, mode=self.mode, shape=self.full_shape)
        self.chunk_index += 1
        self.offset = 0

    def append(self, item):
        if self.current_memmap is None or self.offset >= self.chunk_size:
            self._create_new_memmap()
        self.current_memmap[self.offset] = item
        self.offset += 1
        self.total_items += 1  # 递增总数

    def _save_metadata(self):
        # Save total items, current state, and additional attributes to a JSON file
        metadata = {
            'total_items': self.total_items,
            'chunk_index': self.chunk_index,
            'current_memmap_filename': (
                f"{self.base_filename}_{self.chunk_index - 1}.dat" if self.current_memmap is not None else None),
            'offset': self.offset,
            'dtype': str(self.dtype),  # Save dtype as a string
            'shape_per_item': self.shape_per_item,  # Save shape_per_item as a tuple
            'chunk_size': self.chunk_size  # Save chunk_size as an integer
        }
        with open(self.metadata_filename, 'w') as f:
            json.dump(metadata, f, indent=4)

    def load(self, folder_path='./data'):
        # Load total items, current state, and additional attributes from a JSON file if it exists
        self.folder_path = folder_path
        self.metadata_filename = os.path.join(folder_path, 'metadata.json')

        if os.path.exists(self.metadata_filename):
            with open(self.metadata_filename, 'r') as f:
                metadata = json.load(f)
                # Compare the new dtype with the current self.dtype
                if str(self.dtype) != metadata.get('dtype'):
                    raise ValueError(f"Dtype mismatch: expected {self.dtype}, but metadata contains {metadata.get('dtype')}")
                # Compare the shape_per_item with the current self.shape_per_item
                if self.shape_per_item != tuple(metadata.get('shape_per_item', ())):
                    raise ValueError(f"shape_per_item mismatch: expected {self.shape_per_item}, but metadata contains {tuple(metadata.get('shape_per_item', ()))}")
                self.total_items = metadata.get('total_items', 0)
                self.chunk_index = metadata.get('chunk_index', 0)
                if self.chunk_size != int(metadata.get('chunk_size')):
                    raise ValueError(f"The chunk_size in metadata ({metadata.get('chunk_size')}) does not match the expected value ({self.chunk_size}).")
                current_memmap_filename = metadata.get('current_memmap_filename')
                self.offset = metadata.get('offset', 0)

                # If there's a current memmap filename, we need to re-open it
                if current_memmap_filename is not None:
                    self.current_memmap = np.memmap(os.path.join(self.folder_path, current_memmap_filename),
                                                    dtype=self.dtype, mode='r+',
                                                    shape=(self.chunk_size,) + self.shape_per_item)
                else:
                    self.current_memmap = None

                # Update the full_shape attribute based on the loaded metadata
                self.full_shape = (self.chunk_size,) + self.shape_per_item
        else:
            # If no metadata file exists, initialize the class with default values (or raise an error if appropriate)
            # Note: In this case, we're relying on the __init__ method to set default values for dtype, shape_per_item, etc.
            # However, since we're calling this load method separately, we might want to handle this more gracefully.
            # Or raise an exception if metadata is required.
            raise FileNotFoundError(f"The metadata file at {self.metadata_filename} does not exist.")

    def _get_chunk_and_index(self, global_index):
        """
        Given a global index, determine which chunk and local index it corresponds to.
        """
        chunk_num = global_index // self.chunk_size
        local_index = global_index % self.chunk_size
        return chunk_num, local_index

    def read(self, index):
        """
        Read a single item from the memmap files by its global index.
        Ensures index is within valid range.
        """
        if index >= self.total_items:
            raise IndexError(f"Index {index} out of range (total items: {self.total_items})")

        chunk_num, local_index = self._get_chunk_and_index(index)
        filename = os.path.join(self.folder_path, f"{self.base_filename}_{chunk_num}.dat")
        if not os.path.exists(filename):
            raise IndexError(f"Unexpected error: file {filename} does not exist for index {index}")

        # Open the memmap file and read the item
        memmap = np.memmap(filename, dtype=self.dtype, mode='r', shape=self.full_shape)
        item = memmap[local_index]
        # Note: No need to close the memmap explicitly; it will be garbage collected when no longer referenced.
        return item

    def read_range(self, start, stop):
        """
        Read a range of items from the memmap files.
        Ensures the range is within valid range.
        """
        if start < 0 or stop > self.total_items or start >= stop:
            raise IndexError(f"Range {start} to {stop} out of valid range (0 to {self.total_items})")

        items = []
        for i in range(start, stop):
            try:
                items.append(self.read(i))
            except IndexError as e:
                print(e)  # Optionally handle the error or stop reading
                break
        return np.array(items)

    def clear(self):
        """
        Close the current memmap (if any), reset the class's state, and save updated metadata.
        """
        # Ensure the current memmap (if any) is closed by setting it to None.
        # Note: np.memmap does not have an explicit close method; it relies on garbage collection.
        # Setting to None ensures no further writes will occur, and Python will close the file when the object is garbage collected.
        self.current_memmap = None

        # Reset the class's state
        self.chunk_index = 0
        self.offset = 0
        self.total_items = 0

        # Save the updated metadata
        self._save_metadata()

    def close(self):
        if self.current_memmap is not None:
            # Note: Flushing and closing are handled automatically when the np.memmap object is garbage collected.
            # However, we can explicitly delete it to ensure the file descriptor is closed.
            # Also, save the metadata before closing.
            self._save_metadata()  # 在关闭之前保存总数和状态
            del self.current_memmap
            self.current_memmap = None


if __name__ == '__main__':

    # 使用示例
    # saver = MemmapSaver(dtype=np.float32, shape_per_item=(44, 512), folder_path='./data1')
    saver = MemmapSaver(dtype=np.float32, shape_per_item=(44, 512))

    saver.load()

    # 读取数据
    print(saver.total_items)

    # # 添加一些数据
    # for i in range(500):  # 总共添加35个数据项
    #     data_item = np.random.rand(44, 512).astype(np.float32)
    #     saver.append(data_item)

    # 读取数据
    print(saver.total_items)

    # # 清空数据
    # saver.clear()

    # # 添加一些数据
    # for i in range(10000):  # 总共添加35个数据项
    #     data_item = np.random.rand(32, 512).astype(np.float32)
    #     saver.append(data_item)
    # print(saver.total_items)

    # print(saver.read(2999))
    #
    # some_items = saver.read_range(2997, 2999)
    # print(some_items.shape)
    # print(some_items)

    # for i in range(saver.total_items):
    #     # item = saver.read(i)
    #     # print(item.shape)
    #     # print(item)
    #
    # items = saver.read_range(200, 300)
    # print(items.shape)

    # saver.clear()

    saver.close()

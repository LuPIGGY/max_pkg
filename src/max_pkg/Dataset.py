#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：Dataset.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/21 9:39 
@Brief   ：
"""


from torch.utils.data import Dataset
from Project.max_pkg.MemmapSaver import MemmapSaver


class MemmapDataset(Dataset):
    def __init__(self, saver: MemmapSaver, labels, indexes):
        self.saver = saver
        self.labels = labels
        self.indexes = indexes
        # self.saver.load()

    def __len__(self):
        return len(self.indexes)

    def __getitem__(self, idx):
        return self.saver.read(self.indexes[idx]).copy(), self.labels[self.indexes[idx]]



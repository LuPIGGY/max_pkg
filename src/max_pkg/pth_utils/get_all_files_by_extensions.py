#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：transformer 
@File    ：get_all_files_by_extensions.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/10 16:16 
@Brief   ：
"""

import os


def get_all_files_by_extensions(top_directory, extensions):
    """
    遍历给定目录及其子目录，查找具有指定扩展名列表中任何一个扩展名的文件，
    并返回这些文件路径的列表。

    参数:
    top_directory (str): 要搜索的顶级目录的路径。
    extensions (list of str): 要查找的文件扩展名列表，每个扩展名都应包括点（例如 ['.txt', '.py']）。

    返回:
    list: 匹配文件的路径列表。
    """
    matching_files = []

    # 使用 os.walk 遍历目录树
    for root, dirs, files in os.walk(top_directory):
        for file in files:
            # 检查文件扩展名是否在列表中
            if any(file.endswith(ext) for ext in extensions):
                matching_files.append(os.path.join(root, file))

    return matching_files


if __name__ == "__main__":
    top_dir = r'D:\行为数据——关键点'
    file_extensions = ['.txt', '.py', '.pth']
    file_list = get_all_files_by_extensions(top_dir, file_extensions)
    print('全部.pth文件数量：', len(file_list))
    for file_path in file_list:
        print(file_path)

#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：transformer 
@File    ：find_first_matching_condition.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/10 19:31 
@Brief   ：
"""


def find_first_matching_condition(path, condition_list):
    """
    在条件列表中查找第一个在路径中出现的条件的索引。

    参数:
    path (str): 要检查的路径。
    condition_list (list of str): 包含字符条件的列表。

    返回:
    int or None: 如果找到满足条件的条件，则返回其索引；否则返回 None。
    """
    # 遍历条件列表，检查每个条件是否在路径中
    for index, condition in enumerate(condition_list):
        if condition in path:
            return index  # 如果找到满足条件的条件，返回其索引

    # 如果没有找到满足条件的条件，返回 None
    return None


if __name__ == '__main__':
    # 示例用法
    path = "/home/user/documents/file1.txt"
    condition_list = ["home", "not_here", "documents", "file"]  # 这些是应该检查是否出现在路径中的子字符串条件

    # 调用函数并打印结果
    result_index = find_first_matching_condition(path, condition_list)
    print(result_index)  # 输出: 0，因为 "home" 是第一个在路径中出现的条件

    # 另一个示例，其中没有条件满足（但应该返回第一个检查的索引，如果逻辑是“查找第一个并停止”）
    # 注意：这里的逻辑是，如果没有任何条件满足，则返回 None，因为没有任何条件“首先”满足。
    # 但如果你想要的是“检查到哪个索引为止还没有满足”，则需要稍微调整逻辑（尽管这通常不是这种函数的预期行为）。
    condition_list_no_match = ["not_here", "never_here", "nowhere"]
    result_index_no_match = find_first_matching_condition(path, condition_list_no_match)
    print(result_index_no_match)  # 输出: None，因为没有条件满足

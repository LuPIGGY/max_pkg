#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：ultralytics 
@File    ：get_relative_path.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/14 14:50 
@Brief   ：
"""

import os


def get_relative_path(directory, top_directory):
    """
    获取目标目录相对于顶级目录的相对路径。

    :param directory: 目标目录的路径
    :param top_directory: 顶级目录的路径
    :return: 目标目录相对于顶级目录的相对路径
    """
    relpath = os.path.relpath(directory, top_directory)
    return relpath


# 示例用法
if __name__ == "__main__":
    target_directory = "/home/user/projects/my_project/src"
    top_directory = "/home/user/projects"

    relative_path = get_relative_path(target_directory, top_directory)
    print(f"Relative path: {relative_path}")

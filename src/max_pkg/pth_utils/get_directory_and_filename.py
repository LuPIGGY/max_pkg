#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：ultralytics 
@File    ：get_directory_and_filename.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/14 14:53 
@Brief   ：
"""
import os


def get_directory_and_filename(file_path):
    """
    获取文件路径中的文件名和文件所在目录的路径。

    :param file_path: 文件的完整路径
    :return: 一个元组，包含文件所在目录的路径和文件名
    :raises ValueError: 如果输入路径不是一个文件路径
    """
    # 检查是否为文件路径
    if not os.path.isfile(file_path):
        raise ValueError(f"The provided path '{file_path}' is not a valid file path.")

    directory, filename = os.path.split(file_path)
    return directory, filename


# 示例用法
if __name__ == "__main__":
    pth_file = r"C:\Users\weikunl\CodeRepo\电力\yolo\ultralytics\max_pkg\get_filename.py"

    try:
        directory, file_name = get_directory_and_filename(pth_file)
        print(f"Directory: {directory}")
        print(f"File name: {file_name}")
    except ValueError as e:
        print(e)
#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：transformer 
@File    ：filter_paths_by_any_criterion.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/10 18:38 
@Brief   ：
"""


def filter_paths_by_any_criterion(path_list, criterion_list):
    """
    根据筛选条件列表筛选路径列表，返回满足条件列表中任意条件的路径列表。
    如果条件列表为空，则不进行筛选并返回原始路径列表。

    参数:
    path_list (list of str): 包含路径的列表。
    criterion_list (list of str): 包含筛选条件的字符串列表（每个条件是一个子字符串）。

    返回:
    list of str: 满足条件列表中任意条件的路径列表，或当条件列表为空时的原始路径列表。
    """
    if not criterion_list:  # 如果条件列表为空
        return path_list  # 不进行筛选，直接返回原始路径列表

    # 使用列表推导式筛选路径
    filtered_paths = [
        path for path in path_list
        if any(criterion in path for criterion in criterion_list)
    ]

    return filtered_paths


if __name__ == '__main__':
    # 示例用法
    path_list = [
        "/home/user/documents/file1.txt",
        "/home/user/pictures/photo.jpg",
        "/home/user/music/song.mp3",
        "/var/log/system.log"
    ]

    criterion_list = []  # 筛选条件列表

    # 调用函数并打印结果
    filtered_paths = filter_paths_by_any_criterion(path_list, criterion_list)
    print(filtered_paths)  # 输出: ['/home/user/documents/file1.txt', '/home/user/music/song.mp3', '/var/log/system.log']

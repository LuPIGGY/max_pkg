#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：transformer 
@File    ：check_path_by_conditions.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/10 19:27 
@Brief   ：
"""


def check_path_by_conditions(path, condition_list):
    """
    检查路径是否包含条件列表中的所有字符条件。

    参数:
    path (str): 要检查的路径。
    condition_list (list of str): 包含字符条件的列表。

    返回:
    bool: 如果路径包含所有条件，则返回 True；否则返回 False。
    """
    # 遍历条件列表，检查每个条件是否在路径中
    for condition in condition_list:
        if condition not in path:
            return False  # 如果有一个条件不满足，则返回 False

    # 如果所有条件都满足，则返回 True
    return True


if __name__ == '__main__':
    # 示例用法
    path = "/home/user/documents/file1.txt"
    condition_list = ["home", "documents", "file"]  # 这些是应该出现在路径中的子字符串条件

    # 调用函数并打印结果
    result = check_path_by_conditions(path, condition_list)
    print(result)  # 输出: True，因为所有条件都在路径中

    # 另一个示例，其中一个条件不满足
    condition_list_not_met = ["home", "documents", "not_here"]  # "not_here" 不在路径中
    result_not_met = check_path_by_conditions(path, condition_list_not_met)
    print(result_not_met)  # 输出: False，因为有一个条件不满足

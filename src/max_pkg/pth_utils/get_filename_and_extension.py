#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：get_filename_and_extension.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/18 15:07 
@Brief   ：
"""

import os


def get_filename_and_extension(file_pth):
    """
    从文件路径中分离出文件名和后缀，并检查路径是否指向一个存在的文件。

    参数:
    file_pth (str): 文件路径。

    返回:
    tuple: 如果路径是有效文件，返回包含文件名和后缀的元组（文件名, 后缀）。
           如果路径不是有效文件，返回None或抛出一个异常（根据实现选择）。
    """
    # 检查路径是否指向一个存在的文件
    if not os.path.isfile(file_pth):
        raise ValueError(f"The provided path '{file_pth}' is not a valid file.")

    # 使用os.path.basename获取文件名
    filename = os.path.basename(file_pth)

    # 使用os.path.splitext分离文件名和后缀
    name, extension = os.path.splitext(filename)

    # 去除后缀的点号
    if extension:
        extension = extension[1:]

    return name, extension


if __name__ == '__main__':
    # 示例使用
    try:
        file_pth = r"/max_pkg/pth_utils/get_directory_and_filename.py"
        filename, extension = get_filename_and_extension(file_pth)
        print(f"文件名: {filename}, 后缀: {extension}")
    except ValueError as e:
        print(e)

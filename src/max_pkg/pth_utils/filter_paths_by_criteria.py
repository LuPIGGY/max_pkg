#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：transformer 
@File    ：filter_paths_by_criteria.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/10 18:31 
@Brief   ：
"""


def filter_paths_by_criteria(path_list, criteria_list):
    """
    根据筛选条件列表筛选路径列表。

    参数:
    path_list (list of str): 包含路径的列表。
    criteria_list (list of str): 包含筛选条件的字符串列表。

    返回:
    list of str: 符合所有筛选条件的路径列表。
    """
    if not criteria_list:  # 如果筛选条件列表为空
        # 不进行筛选，直接返回原始路径列表
        return path_list

    # 使用列表推导式筛选路径，确保路径包含所有筛选条件
    filtered_paths = [
        path for path in path_list
        if all(criterion in path for criterion in criteria_list)
    ]

    return filtered_paths


if __name__ == '__main__':
    # 示例用法
    pth_files_list = [
        "/home/user/documents/file1.txt",
        "/home/user/pictures/photo.jpg",
        "/home/user/music/song.mp3",
        "/home/user/movies/drama_film.mp4"
    ]

    filtering_criteria_list = ["home", "user", "file"]  # 注意：这将只筛选出同时包含"home"、"user"和"file"的路径

    # 调用函数并打印结果
    filtered_paths = filter_paths_by_criteria(pth_files_list, filtering_criteria_list)
    print(filtered_paths)  # 输出: ['/home/user/documents/file1.txt']（如果只有这个路径同时满足所有条件）

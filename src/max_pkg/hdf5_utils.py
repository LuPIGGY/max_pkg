#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：hdf5_utils.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/25 12:11 
@Brief   ：
"""

import h5py
import numpy as np


# 定义 HDF5 操作的函数
def open_hdf5_file(filename, mode='w'):
    """打开 HDF5 文件"""
    return h5py.File(filename, mode)


def create_dataset(f, name, shape, maxshape, dtype):
    """创建一个可扩展的数据集"""
    return f.create_dataset(name, shape=shape, maxshape=maxshape, dtype=dtype)


def add_item_to_dataset(dset, item):
    """将项追加到数据集中，支持数组和字符串"""
    current_shape = dset.shape
    dset.resize(current_shape[0] + 1, axis=0)
    dset[-1] = item


def close_hdf5_file(f):
    """关闭 HDF5 文件"""
    f.close()


if __name__ == '__main__':
    # 创建 HDF5 文件
    filename = 'data.h5'
    f = open_hdf5_file(filename, 'w')

    # 创建一个可扩展的数组数据集
    maxshape_array = (None, 32, 500)  # 最大形状，第一个维度可以无限扩展
    data = create_dataset(f, 'data', shape=(0, 32, 500), maxshape=maxshape_array, dtype=np.float32)

    # 创建一个可扩展的字符串数据集
    maxshape_string = (None,)  # 最大形状，第一个维度可以无限扩展
    emotions = create_dataset(f, 'emotions', shape=(0,), maxshape=maxshape_string, dtype=h5py.special_dtype(vlen=str))

    # 每个样本对应的subid
    subs = create_dataset(f, 'subs', shape=(0,), maxshape=(None,), dtype=np.int32)

    # 每个样本对应的视频id
    vids = create_dataset(f, 'vids', shape=(0,), maxshape=(None,), dtype=np.int32)

    # 生成一些示例数据
    num_items = 10  # 假设我们要存入 10 个 (32, 500) 的数组和 10 个字符串
    for i in range(num_items):
        # 生成一个 (32, 500) 的随机数组
        array = np.random.rand(32, 500).astype(np.float32)
        # 将新数组追加到数据集中
        add_item_to_dataset(data, array)
        # 将新字符串追加到数据集中
        add_item_to_dataset(emotions, f"String {i}")
        add_item_to_dataset(subs, i)
        add_item_to_dataset(vids, i)

    # 在这里可以插入其他逻辑
    # 例如，读取数据集的一部分
    # current_shape_array = dset_array.shape
    print("Current shape of the array dataset:", data.shape)
    print("First array in the dataset:")
    print(data[0].shape)

    # current_shape_string = dset_string.shape
    print("Current shape of the emotions dataset:", emotions.shape)
    print("First string in the dataset:")
    print(emotions[0])

    # current_shape_string = subs.shape
    print("Current shape of the subs dataset:", subs.shape)
    print("First string in the dataset:")
    print(subs[0])

    print("Current shape of the vids dataset:", vids.shape)
    print("First string in the dataset:")
    print(vids[0])

    # 关闭 HDF5 文件
    close_hdf5_file(f)

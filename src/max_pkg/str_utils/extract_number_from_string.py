#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：extract_number_from_string.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/25 18:51 
@Brief   ：
"""

import re


def extract_number_from_string(input_string, pattern):
    """
    从输入字符串中提取与给定正则表达式模式匹配的数字。

    参数:
    input_string (str): 要搜索的字符串。
    pattern (str): 用于匹配的正则表达式模式，其中应包含一个捕获组来捕获数字。

    返回:
    int 或 None: 如果找到匹配项，则返回匹配的数字（整数）；否则返回 None。
    """
    match = re.search(pattern, input_string)
    if match:
        # 从捕获组中提取数字字符串
        number_str = match.group(1)
        # 将数字字符串转换为整数并返回
        return int(number_str)
    else:
        # 如果没有找到匹配项，则返回 None
        return None


if __name__ == '__main__':
    # 示例用法
    input_string = "E:\\数据集\\Emotion_dataset\\DEAP\\raw\\sub021"
    pattern = r'sub(\d+)'
    extracted_number = extract_number_from_string(input_string, pattern)

    if extracted_number is not None:
        print(f"从字符串中提取的数字是: {extracted_number}")
    else:
        print("字符串中没有找到匹配的数字")

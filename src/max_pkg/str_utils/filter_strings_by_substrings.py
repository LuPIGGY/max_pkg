#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：filter_strings_by_substrings.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/19 9:14 
@Brief   ：
"""


def filter_strings_by_substrings(strings_list, substrings_list):
    """
    筛选出包含任何子字符串的字符串。

    参数:
    strings_list (list of str): 要筛选的字符串列表。
    substrings_list (list of str): 包含子字符串的列表。

    返回:
    list of str: 满足条件的字符串列表。
    """
    result = []
    for string in strings_list:
        for substring in substrings_list:
            if substring in string:
                result.append(string)
                break  # 如果找到一个匹配项，就跳出内层循环
    return result


if __name__ == '__main__':
    # 示例使用
    strings_list = ['E:\\数据集\\Emotion_dataset\\DEAP\\processed\\s01\\video_0.npz',
                    'E:\\数据集\\Emotion_dataset\\DEAP\\processed\\s01\\video_1.npz',
                    'E:\\数据集\\Emotion_dataset\\DEAP\\processed\\s01\\video_2.npz',
                    'E:\\数据集\\Emotion_dataset\\DEAP\\processed\\s01\\video_3.npz', ]
    substrings_list = ["video_0", "machine", "java"]  # "java" 不会匹配任何字符串

    filtered_list = filter_strings_by_substrings(strings_list, substrings_list)
    print(filtered_list)  # 输出: ['hello world', 'data science with machine learning']

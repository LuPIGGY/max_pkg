#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：filter_strings_by_patterns.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/18 16:21 
@Brief   ：
"""

import re


def filter_strings_by_patterns(strings, patterns):
    """
    根据条件列表中的正则表达式过滤字符串列表，返回满足条件的字符串列表。

    参数:
    strings (list of str): 要过滤的字符串列表。
    patterns (list of str): 包含正则表达式的条件列表。如果为空，则返回原列表。

    返回:
    list of str: 满足条件列表中的任何一个正则表达式的字符串列表，或当patterns为空时返回原列表。
    """
    # 如果patterns列表为空，则直接返回原列表
    if not patterns:
        return strings

    # 编译正则表达式
    compiled_patterns = [re.compile(pattern) for pattern in patterns]

    # 存储满足条件的字符串
    matching_strings = []

    for string in strings:
        for pattern in compiled_patterns:
            if pattern.search(string):
                matching_strings.append(string)
                break  # 如果找到一个匹配项，就无需检查其他模式

    return matching_strings


if __name__ == '__main__':
    # 示例用法
    strings = ["hello123", "world456", "foo789", "bar", "baz123"]
    patterns = [r'\d+']  # 匹配包含数字或包含"foo"的字符串

    matching_strings = filter_strings_by_patterns(strings, patterns)
    print(matching_strings)  # 输出: ['hello123', 'world456', 'foo789', 'baz123']

#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：filter_strings_by_all_patterns.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/18 16:26 
@Brief   ：
"""

import re


def filter_strings_by_all_patterns(strings, patterns):
    """
    根据条件列表中的正则表达式过滤字符串列表，返回满足所有条件的字符串列表。

    参数:
    strings (list of str): 要过滤的字符串列表。
    patterns (list of str): 包含正则表达式的条件列表。如果为空，则返回原列表。

    返回:
    list of str: 满足条件列表中的所有正则表达式的字符串列表，或当patterns为空时返回原列表。
    """
    # 如果patterns列表为空，则直接返回原列表
    if not patterns:
        return strings

    # 编译正则表达式
    compiled_patterns = [re.compile(pattern) for pattern in patterns]

    # 存储满足所有条件的字符串
    matching_strings = []

    for string in strings:
        # 标记当前字符串是否匹配所有模式
        matches_all = True

        for pattern in compiled_patterns:
            if not pattern.search(string):
                matches_all = False
                break  # 如果找到一个不匹配项，就无需检查其他模式

        if matches_all:
            matching_strings.append(string)

    return matching_strings


if __name__ == '__main__':
    # 示例用法
    strings = ["hello123", "worldfoo456", "foo789bar", "bar", "123foo"]
    patterns = [r'\d+', r'l']  # 匹配同时包含数字和"foo"的字符串

    matching_strings = filter_strings_by_all_patterns(strings, patterns)
    print(matching_strings)

    # 输出应该是只包含同时满足两个条件的字符串列表，但在这个例子中可能为空，
    # 因为没有字符串同时包含"\d+"和"foo"作为独立的子串（除非"worldfoo456
    # "这样的字符串被视为满足条件，但它实际上并不满足，因为"foo"和数字"\d+"不是独立的）

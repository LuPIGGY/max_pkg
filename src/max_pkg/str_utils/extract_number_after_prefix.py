#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：emotion_recognition 
@File    ：extract_number_after_prefix.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/21 16:53 
@Brief   ：
"""
import re


class PrefixNumberNotFoundError(ValueError):
    """自定义异常类，用于当未找到前缀后面的数字时抛出。"""
    pass


def extract_number_after_prefix(string, prefix):
    """
    从字符串中提取给定前缀后面的数字，并返回整数。
    注意：这个字符串应该是一个文件路径，且以指定的前缀和数字结尾。

    参数:
    string (str): 包含前缀和数字的字符串（在这个上下文中，通常是一个文件路径）。
    prefix (str): 要搜索的前缀字符串（例如 'sub'）。

    返回:
    int: 前缀后面的数字（如果找到的话）。

    抛出:
    PrefixNumberNotFoundError: 如果未找到前缀后面的数字。
    """
    # 使用前缀构建正则表达式
    pattern = rf'{re.escape(prefix)}(\d+)\.pkl$'

    # 查找前缀后面的数字部分
    match = re.search(pattern, string)

    # 如果找到了匹配项，则提取并转换数字
    if match:
        number = int(match.group(1))
        return number
    else:
        # 抛出自定义异常
        raise PrefixNumberNotFoundError(f"No number found after prefix '{prefix}' in string '{string}'")


if __name__ == '__main__':
    # 示例用法
    string = "E:\\数据集\\Emotion_dataset\\FACED\\Processed_data\\sub000.pkl"
    prefix = "sub"
    try:
        sub_number = extract_number_after_prefix(string, prefix)
        print(sub_number)  # 输出：3
    except PrefixNumberNotFoundError as e:
        print(e)
